String.prototype.replaceAt = function (index, replacement) {
	if (index >= this.length) {
		return this.valueOf();
	}
	return this.substring(0, index) + replacement + this.substring(index + 1);
};

const moodleGroup = {};
let lastLineLength = 0;
for (let i = 0; i < this.file.content.length; i++) {
	lastLineLength++;
	console.log(lastLineLength);
	if (this.file.content.charAt(i) === '\n') {
		if (lastLineLength <= 150) {
			console.log('hier')
			console.log(this.file.content.charAt(i))
			this.file.content.replaceAt(i, '*');
			console.log(this.file.content.charAt(i))
		} else {
			lastLineLength = 0;
		}
	}
}
const lines = this.file.content.split('\n');

for (let i = 1; i < lines.length - 1; i++) {
	const lineContent = lines[i].split(',');
	console.log(lineContent);
	moodleGroup.id = lineContent[0].replaceAll("\"", " ").trim();
	moodleGroup.name = lineContent[1].replaceAll("\"", " ").trim();
	moodleGroup.size = parseInt(lineContent[2].replaceAll("\"", " ").trim());
	moodleGroup.description = (lineContent[3]).replaceAll("\"", " ").trim();
	moodleGroup.members = [];

	console.log(moodleGroup.size)
	for (let j = 0; j < moodleGroup.size; j++) {
		const indexSprung = i * 5;
		const member = {};
		if (indexSprung >= lineContent.length) continue;
		member.username = lineContent[8 + indexSprung].replaceAll("\"", " ").trim();
		member.number = lineContent[9 + indexSprung].replaceAll("\"", " ").trim();
		member.firstName = lineContent[10 + indexSprung].replaceAll("\"", " ").trim();
		member.lastName = lineContent[11 + indexSprung].replaceAll("\"", " ").trim();
		member.email = lineContent[12 + indexSprung].replaceAll("\"", " ").trim();
		moodleGroup.members.push(member);
	}
	this.groups.push(moodleGroup);
}
